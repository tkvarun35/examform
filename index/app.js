var app = angular.module("app", ["ui.router", "ngSanitize"]);
window.baseUrl = "https://35ca-103-101-116-174.ngrok.io/examportal/";

//app config
app.config([
  "$stateProvider",
  function ($state) {
    $state
      .state("loginChoice", {
        url: "/login",
        templateUrl: "../login/loginchoice.html",
        controller: "loginchoiceCtrl",
      })

      .state("facultylogin", {
        url: "/facultylogin",
        templateUrl: "../login/facultylogin.html",
        controller: "facultyloginCtrl",
      })
      .state("facultydashboard", {
        url: "/fdashboard",
        templateUrl: "../dashboard/faculty/facultydashboard.html",
        controller: "facultydashboardCtrl",
      })
      .state("studentlogin", {
        url: "/studentlogin",
        templateUrl: "../login/studentlogin.html",
        controller: "studentloginCtrl",
      })
      .state("studentdashboard", {
        url: "/sdashboard",
        templateUrl: "../dashboard/student/studentdashboard.html",
        controller: "studentdashboardCtrl",
      })

      .state("createExam", {
        url: "/createexam",
        templateUrl: "../dashboard/faculty/createexam.html",

        controller: "createExamCtrl",
      })
      .state("evaluateExam", {
        url: "/evaluateexam",
        templateUrl: "../dashboard/faculty/evaluateexam.html",

        controller: "evaluateExamCtrl",
      })
      .state("viewans", {
        url: "/viewans",
        templateUrl: "../dashboard/faculty/viewans.html",

        controller: "evaluateExamCtrl",
      })
      .state("viewques", {
        url: "/viewques",
        templateUrl: "../dashboard/faculty/viewques.html",

        controller: "createExamCtrl",
      })
      .state("logout", {
        controller: "logoutCtrl",
      })
      .state("logout2", {
        controller: "logout2Ctrl",
      })
      .state("attempt", {
        url: "/test",
        templateUrl: "../dashboard/student/question.html",

        controller: "testCtrl",
      })
      .state("otherwise", {
        url: "/login",
        templateUrl: "../login/loginchoice.html",
      });
  },
]);

//login controller
app.controller("loginchoiceCtrl", function ($scope, $state, $http) {
  $scope.$parent.navbar = false;
});

//logout controller
app.controller(
  "logoutCtrl",
  function ($scope, $state, $http, $location, $window) {
    Swal.fire({
      title: "Are you sure?",

      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Log Out",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire("LoggedOut!", "success");
        $scope.$parent.navbar = false;

        $http({
          url: window.baseUrl + "logout",
          method: "GET",
          withCredentials: true,
        }).then(function (response) {
          $location.path("/login");
        });
      } else {
      }
    });
  }
);

//separate logout controller for unauthorized logout
app.controller(
  "logout2Ctrl",
  function ($scope, $state, $http, $location, $window) {
    $http({
      url: window.baseUrl + "logout",
      method: "GET",
      withCredentials: true,
    }).then(function (response) {
      $location.path("/login");
    });
  }
);

//faculty dashboard controller
app.controller(
  "facultydashboardCtrl",
  function ($scope, $state, $http, $location) {
    $http({
      url: window.baseUrl + "get_examforms",
      method: "GET",
      withCredentials: true,
    }).then(
      function (response) {
        $scope.$parent.teachericon = true;
        $scope.$parent.studenticon = false;
        $scope.$parent.navbar = true;
        $scope.$parent.teacher1 = window.teacher;
      },
      function (response) {
        if (response.status == 401) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "You are not authorized for the page",
          });
          $state.go("logout2");
        } else {
          $location.path("/login");
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "You are not logged in",
          });
        }
      }
    );
  }
);

//faculty login controller
app.controller("facultyloginCtrl", function ($scope, $state, $http, $location) {
  $http({
    url: window.baseUrl + "get_examforms",
    method: "GET",
    withCredentials: true,
  }).then(
    function (response) {
      $state.go("facultydashboard");
    },
    function (response) {
      if (response.status == 401) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "You are not authorized for the page",
        });
        $state.go("logout2");
      } else {
        $scope.$parent.navbar = false;
        $scope.flogin = function () {
          $scope.loading3 = true;
          var faculty = {
            username: $scope.username,
            password: $scope.password,
          };
          $http({
            url: window.baseUrl + "faculty_login",
            method: "POST",
            withCredentials: true,
            data: faculty,
          }).then(
            function (response) {
              $scope.loading3 = false;
              if (response.status == "200") {
                window.teacher =
                  response.data[0].first_name +
                  " " +
                  response.data[0].last_name;
                $scope.$parent.teacher1 = window.teacher;

                $scope.$parent.navbar = true;
                $location.path("/fdashboard");

                Swal.fire({
                  position: "top-end",
                  icon: "success",
                  title: "Logged In as " + window.teacher,
                  showConfirmButton: false,
                  timer: 1500,
                });
              }
            },
            function (response) {
              Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "Something went wrong!",
              });
              $scope.loading3 = false;
            }
          );
        };
      }
    }
  );
});

// faculty exam controller for creating exam
app.controller("createExamCtrl", function ($scope, $state, $http, $location) {
  $http({
    url: window.baseUrl + "get_examforms",
    method: "GET",
    withCredentials: true,
  }).then(
    function (response) {
      $scope.$parent.teacher1 = window.teacher;
      $scope.$parent.navbar = true;
      $scope.$parent.teachericon = true;
      $scope.$parent.studenticon = false;
      $scope.createoption = false;

      $scope.dropdown1 = "...";
      $scope.dropdown2 = "...";
      $scope.branch = true;

      $http({
        url: window.baseUrl + "get_courses",
        method: "GET",
        withCredentials: true,
      }).then(function (response) {
        $scope.loading6 = true;
        $scope.courses = response.data;
      });
      $scope.select = function (id, name) {
        $scope.dropdown1 = name;
        $scope.dropdown2 = "...";

        $scope.branch = false;
        var courses = {
          s_no: id,
        };
        $http({
          url: window.baseUrl + "get_branches",
          method: "GET",
          withCredentials: true,
          params: courses,
        }).then(function (response) {
          $scope.loading5 = true;
          $scope.branches = response.data;
        });
      };
      $scope.select2 = function (branch) {
        $scope.dropdown2 = branch;
      };
      $scope.status = false;
      $scope.view = function () {
        $http({
          url: window.baseUrl + "get_examforms",
          method: "GET",
          withCredentials: true,
        }).then(function (response) {
          $scope.loading2 = true;

          $scope.exams = response.data;
        });
        if ($scope.status == true) {
          $scope.status = false;
        } else {
          $scope.status = true;
        }
      };
      $scope.viewques = function (id) {
        window.updateId = id;
        $http({
          url: window.baseUrl + "view_exam",
          method: "GET",
          withCredentials: true,
          params: { id: id },
        }).then(function (response) {
          $location.path("/viewques");
          window.updatebutton1 = true;

          window.ques = response.data[0].question;
        });
      };
      $scope.updatebutton = window.updatebutton1;
      $scope.reciquestion = window.ques;

      $scope.create = function () {
        function pad2(n) {
          return (n < 10 ? "0" : "") + n;
        }

        var date = $scope.sdate;
        var month = pad2(date.getMonth() + 1); //months (0-11)
        var day = pad2(date.getDate()); //day (1-31)
        var year = date.getFullYear();

        var startdate = year + "-" + month + "-" + day;

        var date = $scope.edate;
        var month = pad2(date.getMonth() + 1); //months (0-11)
        var day = pad2(date.getDate()); //day (1-31)
        var year = date.getFullYear();

        var enddate = year + "-" + month + "-" + day;

        starttime = $scope.stime;
        sttime = starttime.toLocaleTimeString();

        endtime = $scope.etime;
        ettime = endtime.toLocaleTimeString();

        var form = {
          course: $scope.dropdown1,
          branch: $scope.dropdown2,
          subject: $scope.subject,
          exam_title: $scope.title,
          start_date: startdate,
          end_date: enddate,
          start_time: sttime,
          end_time: ettime,
        };
        $scope.loading7 = true;

        if (enddate > startdate) {
          $http({
            url: window.baseUrl + "create_form",
            method: "POST",
            withCredentials: true,
            data: form,
          }).then(function (response) {
            $scope.loading7 = false;
            $scope.createoption = true;
            $scope.id = response.data;

            window.id = $scope.id[0].id;
            $scope.dropdown1 = "...";
            $scope.dropdown2 = "...";
            $scope.subject = "";
            $scope.title = "";
            $scope.sdate = null;
            $scope.edate = null;
            $scope.stime = null;
            $scope.etime = null;
            $scope.branch = true;
            $scope.testcourse = response.data[0].exam_title;
            $scope.testsubject = response.data[0].subject;
            Swal.fire({
              position: "top-end",
              icon: "success",
              title: "Form Created",
              showConfirmButton: false,
              timer: 1000,
            });
          });
        } else {
          $scope.loading7 = false;
          Swal.fire("", "End Date is before Start Date", "question");
        }
      };

      $scope.update = function () {
        var updatequestion = {
          id: window.updateId,
          question: $scope.reciquestion,
        };

        Swal.fire({
          title: "Do you want to save the changes?",
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: "Save",
          denyButtonText: `Don't save`,
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire("Saved!", "", "success");
            $http({
              url: window.baseUrl + "update_exam",
              method: "POST",
              withCredentials: true,
              data: updatequestion,
            }).then(function (response) {
              $location.path("/createexam");
            });
          } else if (result.isDenied) {
            Swal.fire("Changes are not saved", "", "info");
          }
        });
      };

      $scope.fields = [];

      let index = 0;
      var a = 0;

      $scope.addBloc = function () {
        $scope.fields.push((test = {}));

        index++;
        a++;
      };
      $scope.removeBloc = function (ques) {
        var index = $scope.fields.indexOf(ques);
        $scope.fields.splice(index, 1);
      };
      $scope.submit = function () {
        let i = 0;
        while (i < $scope.fields.length) {
          $scope.fields[i].ans = "";
          i++;
        }
        var question = {
          id: window.id,
          question: $scope.fields,
        };

        Swal.fire({
          title: "Do you want to save the test paper?",
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: "Save",
          denyButtonText: `Don't save`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            $http({
              url: window.baseUrl + "create_exam",
              method: "POST",
              withCredentials: true,
              data: question,
            }).then(function (response) {
              $scope.view();
              $scope.createoption = false;
              Swal.fire({
                position: "top-end",
                icon: "success",
                title: "Qustions Added",
                showConfirmButton: false,
                timer: 1000,
              });
              Swal.fire("Saved!", "", "success");
            });
          } else if (result.isDenied) {
            Swal.fire("Changes are not saved", "", "info");
          }
        });
      };

      $scope.del = function (id) {
        Swal.fire({
          title: "Are you sure?",
          text: "You won't be able to revert this!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes, delete it!",
        }).then((result) => {
          if (result.isConfirmed) {
            $http({
              url: window.baseUrl + "delete_examform",
              method: "GET",
              withCredentials: true,
              params: { id: id },
            }).then(function (response) {
              $scope.view();
            });
            Swal.fire("Deleted!", "Your file has been deleted.", "success");
          }
        });
      };
    },
    function (response) {
      if (response.status == 401) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "You are not authorized for the page",
        });
        $state.go("logout2");
      } else {
        $location.path("/login");
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "You are not logged in",
        });
      }
    }
  );
});

//faculty evaluation controller for evaluating exam answers
app.controller("evaluateExamCtrl", function ($scope, $state, $http, $location) {
  $http({
    url: window.baseUrl + "get_examforms",
    method: "GET",
    withCredentials: true,
  }).then(
    function (response) {
      $scope.$parent.teacher1 = window.teacher;

      $scope.$parent.navbar = true;

      $scope.$parent.teachericon = true;
      $scope.$parent.studenticon = false;

      $http({
        url: window.baseUrl + "evaluate_exams",
        method: "GET",
        withCredentials: true,
      }).then(function (response) {
        $scope.loading1 = true;
        $scope.exams = response.data;
      });

      $scope.evaluate1 = function (id) {
        window.viewid = id;
        $http({
          url: window.baseUrl + "evaluate",
          method: "GET",
          withCredentials: true,
          params: { id: id },
        }).then(function (response) {
          $scope.table = true;

          $scope.evaluate = response.data[0];
          window.evaluate1 = response.data;

          $scope.title1 =
            response.data[1][0].subject + "-" + response.data[1][0].exam_title;
        });
      };

      $scope.viewans = function (id) {
        window.ansid = id;
        window.first_name1 = window.evaluate1[0][window.ansid].first_name;
        window.mobileno1 = window.evaluate1[0][window.ansid].mobile_no;
        window.user_name1 = window.evaluate1[0][window.ansid].username;
        window.studentid1 = window.evaluate1[0][window.ansid].student_id;

        $http({
          url: window.baseUrl + "view_ans",
          method: "GET",
          withCredentials: true,
          params: { user_id: id, paper_id: window.viewid },
        }).then(function (response) {
          window.recievedanswer = response.data[0].ans;
          $location.path("/viewans");
        });
      };

      $scope.recianswer = window.recievedanswer;

      $scope.studentfname = window.first_name1;
      $scope.studentuser_name = window.user_name1;
      $scope.studentid = window.studentid1;
      $scope.studentmobileno = window.mobileno1;
    },
    function (response) {
      if (response.status == 401) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "You are not authorized for the page",
        });
        $state.go("logout2");
      } else {
        $location.path("/login");
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "You are not logged in",
        });
      }
    }
  );
});

//student login controller
app.controller("studentloginCtrl", function ($scope, $http, $location, $state) {
  $http({
    url: window.baseUrl + "live_exams",
    method: "GET",
    withCredentials: true,
  }).then(
    function (response) {
      if (response.status == 200) {
        $state.go("studentdashboard");
      }
    },
    function (response) {
      if (response.status == 401) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "You are not authorized for the page",
        });
        $state.go("logout2");
      } else {
        $scope.$parent.navbar = false;
        $scope.flogin = function () {
          $scope.loading4 = true;
          var students = {
            username: $scope.username,
            password: $scope.password,
          };
          $http({
            url: window.baseUrl + "student_login",
            method: "POST",
            withCredentials: true,
            data: students,
          }).then(
            function (response) {
              $scope.loading4 = false;
              if (response.status == "200") {
                window.student =
                  response.data[0].first_name +
                  " " +
                  response.data[0].last_name;
                $scope.$parent.student1 = window.student;

                $location.path("/sdashboard");
                $scope.$parent.navbar = true;
                Swal.fire({
                  position: "top-end",
                  icon: "success",
                  title: "Logged In as " + window.student,
                  showConfirmButton: false,
                  timer: 1500,
                });
              } else {
                alert("Invalid Login");
              }
            },
            function (response) {
              Swal.fire({
                icon: "error",
                title: "Oops...",
                text: "Something went wrong!",
              });
              $scope.loading4 = false;
            }
          );
        };
      }
    }
  );
});

// student dashboard controller
app.controller(
  "studentdashboardCtrl",
  function ($scope, $state, $http, $location) {
    $http({
      url: window.baseUrl + "live_exams",
      method: "GET",
      withCredentials: true,
    }).then(
      function (response) {
        $scope.$parent.student1 = window.student;

        $scope.$parent.teachericon = false;
        $scope.$parent.studenticon = true;
        $scope.$parent.navbar = true;
        window.loading = false;

        $http({
          url: window.baseUrl + "live_exams",
          method: "GET",
          withCredentials: true,
        }).then(function (response) {
          $scope.live = true;

          $scope.liveexam = response.data;
          window.liveexam = response.data;
        });

        $http({
          url: window.baseUrl + "upcoming_exams",
          method: "GET",
          withCredentials: true,
        }).then(function (response) {
          $scope.upcoming = true;

          $scope.upcomingexam = response.data;
        });

        $http({
          url: window.baseUrl + "previous_exams",
          method: "GET",
          withCredentials: true,
        }).then(function (response) {
          $scope.previous = true;

          $scope.previousexam = response.data;
        });
        $scope.attempt = function (id, index) {
          window.id2 = id;
          $http({
            url: window.baseUrl + "get_ques",
            method: "GET",
            withCredentials: true,
            params: { id: id },
          }).then(function (response) {
            window.liveexam2 =
              window.liveexam[index].end_date +
              " " +
              window.liveexam[index].end_time;

            $scope.attempt = response.data;
            window.submittest = true;

            window.attemptques = $scope.attempt[0].question;

            $location.path("/test");
          });
        };
      },
      function (response) {
        if (response.status == 401) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "You are not authorized for the page",
          });
          $state.go("logout2");
        } else {
          $location.path("/login");
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "You are not logged in",
          });
        }
      }
    );
  }
);

// student test controller for test management
app.controller("testCtrl", function ($scope, $state, $http, $location) {
  $http({
    url: window.baseUrl + "live_exams",
    method: "GET",
    withCredentials: true,
  }).then(
    function (response) {
      $scope.$parent.student1 = window.student;
      $scope.attemptquestion = window.attemptques;
      $scope.submit = window.submittest;

      var countDownDate = new Date(window.liveexam2).getTime();

      var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        $scope.timer =
          days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

        if (distance < 0) {
          clearInterval(x);
          $scope.timer = "EXPIRED";
          $scope.TestSubmit();
        }
        $scope.$apply();
      }, 1000);

      $scope.TestSubmit = function () {
        var answer = {
          id: window.id2,
          question: $scope.attemptquestion,
        };
        Swal.fire({
          title: "Do you want to submit the test?",
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: "Save",
          denyButtonText: `Don't save`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            $http({
              url: window.baseUrl + "attempt_exam",
              method: "POST",
              withCredentials: true,
              data: answer,
            }).then(function (response) {
              $location.path("/sdashboard");
            });
            Swal.fire("Saved!", "", "success");
          } else if (result.isDenied) {
            Swal.fire("Changes are not saved", "", "info");
          }
        });
      };
    },
    function (response) {
      if (response.status == 401) {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "You are not authorized for the page",
        });
        $state.go("logout2");
      } else {
        $location.path("/login");
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "You are not logged in",
        });
      }
    }
  );
});
